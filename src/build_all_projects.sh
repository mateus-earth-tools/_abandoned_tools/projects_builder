#!/usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : build_all_projects.sh                                         ##
##  Project   : projects_builder                                              ##
##  Date      : Jun 15, 2020                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2020                                                ##
##                                                                            ##
##  Description :                                                             ##
##                                                                            ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
SCRIPT_DIR="$(pw_get_script_dir)";


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
gitlab()
{
    echo "https://gitlab.com/stdmatt-$1";
}

##------------------------------------------------------------------------------
build()
{
    ## If we pass a name of the game that we want to build
    ## just build that game and nothing else.
    if [ -n "$GAME" ]; then
        if [ "$1" != "$GAME" ]; then
            return;
        fi;
    fi;

    "${SCRIPT_DIR}/build_project.sh" $1 $2 $3;
}


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
GAME="$1";

##    Name           Type    Gitlab Path
build metaballs      demos   $(gitlab demos/metaballs);
build simple_clock   demos   $(gitlab demos/simple_clock);
build simple_tree    demos   $(gitlab demos/simple_tree);
build starfield      demos   $(gitlab demos/starfield);
build game_of_life   demos   $(gitlab demos/game_of_life);
##
##    Name               Type    Gitlab Path
build el_jamon_volador   games   $(gitlab games/el_jamon_volador);
build color_grid         games   $(gitlab games/color_grid);
build nuclear_rain       games   $(gitlab games/nuclear_rain);
build simple_snake       games   $(gitlab games/simple_snake);
build cosmic_intruders   games   $(gitlab games/cosmic_intruders);
##
##    Name            Type    Gitlab Path
build kaboom          games   $(gitlab games/old_old_old_games/kaboom);
build ramit           games   $(gitlab games/old_old_old_games/ramit);
build taz             games   $(gitlab games/old_old_old_games/taz);
build space_raiders   games   $(gitlab games/old_old_old_games/space_raiders);
